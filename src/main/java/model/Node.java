package model;

import main.NodeData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Point2D;

public class Node implements NodeData
{
    public static List<Node> listFromJSON(JSONArray arr, String parentId) {
        List<Node> res = new ArrayList<>();
        if (arr == null) return res;

        for (Object o : arr) {
            res.add(new Node((JSONObject) o, parentId));
        }

        return res;
    }

    public static JSONArray listToJSON(List<Node> l) {
        JSONArray res = new JSONArray();
        if (l == null) return res;

        for (Node n : l) {
            res.add(n.toJSON());
        }

        return res;
    }

    private String label;
    private String image;
    private String id;
    private String parentId;
    private Integer color;
    private Integer visibleColor;
    private String sticker;
    private List<Node> children = new ArrayList<>();
    private Point2D position;

    public Node(String label, String image, String parentId) {
        this.id = MindMap.produceNewID();
        this.label = label;
        this.image = image;
        this.parentId = parentId;
    }

    public Node(JSONObject o, String parentId) {
        this.id = MindMap.produceNewID();
        this.parentId = parentId;

        label = (String) o.get("label");
        image = (String) o.get("image");
        sticker = (String) o.get("sticker");
        
        try
        {
            this.position = new Point2D((double)o.get("x"), (double)o.get("y"));
        }
        catch (Exception e)
        {
            
        }
        Long cval = (Long) o.get("color");
        color = cval != null ? cval.intValue() : null;
        JSONArray arr = (JSONArray) o.get("children");
        children.addAll(Node.listFromJSON(arr, id));
    }
    
    public String getParentId()
    {
        return parentId;
    }
    
    public String getLabel() 
    {
        return label;
    }

    public List<Node> getChildren()
    {
        return children;
    }

    public String getImage() {
        return image;
    }

    public String getSticker() { return sticker; }

    public String getID()
    {
        return id;
    }

    public Integer getColor() { return color; }
    public Integer getVisibleColor() { return visibleColor; }

    public void setLabel(String label)
    {
        this.label=label;
    }

    public void setId(String id)
    {
        this.id=id;
    }

    public void setImage(String image) {this.image=image;}
    public void setSticker(String sticker) { this.sticker = sticker; }

    public void setColor(Integer color) { this.color = color; }
    public void setVisibleColor(Integer color) { this.visibleColor = color; }

    public void setParentID(String parentID)
    {
        parentId = parentID;
    }

    public void addChild(int position, Node child)
    {
        children.add(position, child);
    }
    
    public void addChild(Node child)
    {
        children.add(child);
    }
    
    public void removeChild(Node child)
    {
        children.remove(child);
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public String toString() {
        return toJSON().toJSONString();
    }

    public JSONObject toJSON() {
        JSONObject res = new JSONObject();

        res.put("label", label);

        if (color != null) res.put("color", color);
        if (image != null) res.put("image", image);
        if (sticker != null) res.put("sticker", sticker);

        res.put("x", position.getX());
        res.put("y", position.getY());
        if (children.size() > 0) res.put("children", Node.listToJSON(children));

        return res;
    }

    public boolean hasDescendant(String id) {
        for (Node x : this.children) {
            if (x.getID().equals(id) || x.hasDescendant(id))
                return true;
        }

        return false;
    }
    
    public void setPosition(Point2D newPosition)
    {
        this.position = newPosition;
    }
    
    public Point2D getPosition()
    {
        return this.position;
    }
}