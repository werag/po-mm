package model;

import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import javafx.geometry.Point2D;

import main.EditDescription;
import main.UpdateReceiver;
import org.json.simple.parser.ParseException;

public class Model 
{
    private MindMap mindMap;
    private boolean dirty;
    private ArrayList<UpdateReceiver> receivers = new ArrayList<>();
    
    public Model() {
        newMindMap();
    }
    
    // Rejestruje nasłuchiwanie na zmiany w modelu
    public void requestUpdates(UpdateReceiver ur) {
        receivers.add(ur);
    }

    private void pushUpdateToAll() {
        for (UpdateReceiver ur : receivers) {
            ur.pushUpdate(mindMap);
        }
    }

    public void loadFile(String path) throws ParseException, IOException {
        FileReader fileReader = new FileReader(path);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        StringBuilder content = new StringBuilder();
        String line;

        while((line = bufferedReader.readLine()) != null) {
            content.append(line);
        }

        bufferedReader.close();
        mindMap = new MindMap(content.toString());
        mindMap.setFilePath(path);
        dirty = false;
        pushUpdateToAll();
    }
    
    public void saveFile(String path) throws IOException {
        if (path == null) path = mindMap.getFilePath();
        else mindMap.setFilePath(path);

        FileWriter file = new FileWriter(path);
        file.write(mindMap.toString());
        file.flush();
        file.close();
        dirty = false;
    }

    public void newMindMap() {
        mindMap = new MindMap();
        dirty = false;
        pushUpdateToAll();
    }

    public void renameMindMap(String title)
    {
        mindMap.setTitle(title);
        dirty = true;
        pushUpdateToAll();
    }

    public void addNode(String parentId, EditDescription description) 
    {
        mindMap.addNode(parentId, description);
        dirty = true;
        pushUpdateToAll();
    }
    
    public void editNode(String id, EditDescription description) 
    {
        mindMap.editNode(id, description);
        if (description.toRender) {
            dirty = true;
            pushUpdateToAll();
        }
    }
    
    public void removeNode(String id) 
    {
        mindMap.removeNode(id);
        dirty = true;
        pushUpdateToAll();
    }
    
    public void reorderNode(String id, int position) 
    {
        mindMap.reorderNode(id, position);
        dirty = true;
        pushUpdateToAll();
    }
    
    public void reattachNode(String id, String newParentId, Integer position)
    {
        mindMap.reattachNode(id, newParentId, position);
        dirty = true;
        pushUpdateToAll();
    }

    public boolean isDirty() {
        return dirty;
    }
}
