package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import exceptions.WrongIDException;
import javafx.geometry.Point2D;
import main.DataChange;
import main.EditDescription;
import main.MindMapData;
import main.NodeData;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@SuppressWarnings("ALL")
public class MindMap implements MindMapData {
    private static Integer idCounter;
    private String filePath;
    private String title;

    private Map<String, Node> nodes = new HashMap<>();
    private List<Node> nodesRight = new ArrayList<>();
    private List<Node> nodesLeft = new ArrayList<>();
    private Map<String, Point2D> positions = new HashMap<>();
    private boolean typeOfLayout = false;
    private Point2D rootsPosition;

    public MindMap() {
        title = "Tytuł";
        filePath = null;
    }

    public MindMap(String content) throws ParseException {
        this();
        JSONParser parser = new JSONParser();
        fromJSON((JSONObject) parser.parse(content));
        inheritColors(null);
    }

    private void fromJSON(JSONObject content) {
        title = (String) content.get("title");
        if (content.containsKey("layout"))
            typeOfLayout = (boolean) content.get("layout");
        JSONObject nb = (JSONObject) content.get("nodes");
        nodesLeft = Node.listFromJSON((JSONArray) nb.get("left"), "");
        nodesRight = Node.listFromJSON((JSONArray) nb.get("right"), "");
        if (content.containsKey("rootx"))
            rootsPosition = new Point2D((double) content.get("rootx"), (double) content.get("rooty"));
        fillMap();
    }

    private void fillMap() {
        fillMap(nodesLeft);
        fillMap(nodesRight);
    }

    private void fillMap(List<Node> l) {
        for (Node n : l) {
            nodes.put(n.getID(), n);
            fillMap(n.getChildren());
        }
    }

    private void inheritColors(Integer color) {
        inheritColors(nodesLeft, color);
        inheritColors(nodesRight, color);
    }

    private void inheritColors(List<Node> l, Integer color) {
        for (Node x : l) {
            Integer cc = color;
            if (x.getColor() != null) {
                cc = x.getColor();
            }

            x.setVisibleColor(cc);
            inheritColors(x.getChildren(), cc);
        }
    }

    public String toString() {
        JSONObject res = new JSONObject();
        res.put("title", title);

        JSONObject n = new JSONObject();
        JSONArray nLeft = Node.listToJSON(nodesLeft);
        JSONArray nRight = Node.listToJSON(nodesRight);
        n.put("left", nLeft);
        n.put("right", nRight);
        res.put("layout", typeOfLayout);
        res.put("rootx", rootsPosition.getX());
        res.put("rooty", rootsPosition.getY());
        res.put("nodes", n);

        return res.toJSONString();
    }

    public void reorderNode(String ID, int position) {
        try {
            if (!nodes.containsKey(ID))
                throw new WrongIDException(ID);
        }
        catch(WrongIDException e){
            System.out.println(e);
        }
        Node nodeToReorder = nodes.get(ID);
        Node parent = nodes.get(nodeToReorder.getParentId());
        parent.removeChild(nodeToReorder);
        parent.addChild(position, nodeToReorder);
    }

    public void reattachNode(String ID, String newParentID, Integer position) {
        try { if (!nodes.containsKey(ID)) throw new WrongIDException(ID);
        } catch (WrongIDException e) {
            System.out.println(e);
        }

        Node node = nodes.get(ID);
        if (node.getParentId()
            .equals(newParentID)) return;
        if (node.hasDescendant(newParentID)) return;

        if (!newParentID.equals("")) {
            Node newParent = nodes.get(newParentID);
            newParent.addChild(node);
            ArrayList<Node> s = new ArrayList<>();
            s.add(node);
            inheritColors(s, newParent.getColor());
        } else {
            if (nodesLeft.size() <= nodesRight.size()) {
                nodesLeft.add(node);
            } else {
                nodesRight.add(node);
            }
        }

        if (!node.getParentId().equals("")) {
            Node oldParent = nodes.get(node.getParentId());
            oldParent.removeChild(node);
        } else {
            nodesLeft.remove(node);
            nodesRight.remove(node);
        }

        node.setParentID(newParentID);
        if (position != null) reorderNode(ID, position);
    }

    public void removeNode(String ID) {
        try{
            if (!nodes.containsKey(ID))
                throw new WrongIDException(ID);
        } catch (WrongIDException e) {
            System.out.println(e);
        }
        Node nodeToDelete = nodes.get(ID);
        String parentID = nodeToDelete.getParentId();

        ArrayList<Node> nodesToDelete = new ArrayList<Node>(nodeToDelete.getChildren()); // nie chcemy się iterować po liście, z której podczas iteracji usuwamy elementy, dlatego tworzymy kopię
        for (Node n: nodesToDelete) removeNode(n.getID());
        nodes.remove(ID);

        if (!parentID.equals("")) {
            Node parent = nodes.get(parentID);
            parent.removeChild(nodeToDelete);
        } else {
            nodesLeft.remove(nodeToDelete);
            nodesRight.remove(nodeToDelete);
        }
    }

    public static String produceNewID() {
        if(idCounter==null)
            idCounter=0;
        else
            idCounter++;
        return idCounter.toString();
    }

    public void addNode (String parentID, EditDescription description) {
        String label = "";

        Node newNode = new Node(label, null, parentID);
        applyDescription(newNode, description);

        nodes.put(newNode.getID(), newNode);
        if (!parentID.equals("")) {
            Node parent = nodes.get(parentID);
            parent.addChild(newNode);
            ArrayList<Node> s = new ArrayList<>();
            s.add(newNode);
            inheritColors(s, parent.getColor());
        } else {
            if (nodesLeft.size() <= nodesRight.size()) {
                nodesLeft.add(newNode);
            } else {
                nodesRight.add(newNode);
            }
        }
    }

    public void editNode(String ID, EditDescription description) {
        if (ID.equals(""))
        {
            rootsPosition = description.position.getContent();
            return;
        }
        Node nodeToEdit = nodes.get(ID);
        applyDescription(nodeToEdit, description);
    }

    private void applyDescription(Node node, EditDescription description) {
        if (description.label != null) {
            String newContent = description.label.getContent();
            node.setLabel(newContent);
        }

        if (description.image != null) {
            if (description.image.getType() == DataChange.EDIT) {
                String newImage = description.image.getContent();
                node.setImage(newImage);
            } else {
                node.setImage(null);
            }
        }

        if (description.color != null) {
            if (description.color.getType() == DataChange.EDIT) {
                Integer cc = description.color.getContent();
                node.setColor(cc);
                if (cc == null && nodes.containsKey(node.getParentId())) {
                    cc = nodes.get(node.getParentId()).getVisibleColor();
                }

                node.setVisibleColor(cc);
                inheritColors(node.getChildren(), cc);
            } else {
                node.setColor(null);
            }
        }
        if (description.sticker != null && description.sticker.getType() == DataChange.EDIT) {
            node.setSticker(description.sticker.getContent());
        }
        if (description.layout != null)
            this.typeOfLayout = description.layout;
        if (description.position != null)
        {
            if (node.getID().equals(""))
                rootsPosition = description.position.getContent();
            node.setPosition(description.position.getContent());  
        }
    }

    @Override
    public String getFilePath() {
        return filePath;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public List<? extends NodeData> getChildren() {
        List<Node> r = new ArrayList<>(nodesLeft);
        r.addAll(nodesRight);
        return r;
    }

    @Override
    public List<? extends NodeData> getLeftChildren() { return nodesLeft; }
    @Override
    public List<? extends NodeData> getRightChildren() { return nodesRight; }
    
    public boolean getTypeOfLayout()
    {
        return typeOfLayout;
    }

    public void setTypeOfLayout(boolean typeOfLayout)
    {
        this.typeOfLayout = typeOfLayout;
    }
    
    public Point2D getRootsPosition()
    {
        return this.rootsPosition;
    }
}