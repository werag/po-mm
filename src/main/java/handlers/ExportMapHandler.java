package handlers;

public interface ExportMapHandler {
    void handle(String path, String content);
}
