package handlers;

import main.EditDescription;

public interface EditedHandler {
    void handle(String id, EditDescription description);
}
