package handlers;

import main.EditDescription;

public interface NodeCreatedHandler {
    void handle(String parentId, EditDescription description);
}
