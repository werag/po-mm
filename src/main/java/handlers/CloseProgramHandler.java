package handlers;

import javafx.stage.WindowEvent;

public interface CloseProgramHandler {
    void handle(WindowEvent event);
}
