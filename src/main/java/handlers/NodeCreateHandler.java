package handlers;

public interface NodeCreateHandler {
    void handle(String parentId);
}
