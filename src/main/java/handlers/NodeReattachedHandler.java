package handlers;

public interface NodeReattachedHandler {
    void handle(String id, String parentId, Integer position);
}
