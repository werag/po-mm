package handlers;

import javafx.event.ActionEvent;

public interface ChangeLayoutHandler
{
    void handle(ActionEvent e);
}
