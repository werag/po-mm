package handlers;

public interface PreSaveMapHandler {
    void handle(boolean getPath);
}
