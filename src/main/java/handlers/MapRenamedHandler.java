package handlers;

public interface MapRenamedHandler {
    void handle(String newName);
}
