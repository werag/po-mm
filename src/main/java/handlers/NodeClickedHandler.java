package handlers;

public interface NodeClickedHandler {
    public void handle(String id);
}
