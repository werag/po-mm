package handlers;

import org.json.simple.parser.ParseException;

import java.io.IOException;

public interface OpenMapHandler {
    public void handle(String path);
}
