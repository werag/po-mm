package handlers;

public interface SaveMapHandler {
    public void handle(String path);
}
