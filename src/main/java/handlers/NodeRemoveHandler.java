package handlers;

public interface NodeRemoveHandler {
    void handle(String id);
}
