package ui;

import handlers.EditCanceledHandler;
import handlers.EditedHandler;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import main.EditDescription;

public abstract class Edit extends VBox {
    EditDescription changes = new EditDescription();

    private HBox buttonBar = new HBox();
    private Button cancelButton = new Button("Anuluj");
    private Button saveButton = new Button("Zapisz");

    private EditCanceledHandler onCloseHandler;
    private EditedHandler onSaveHandler;

    {
        cancelButton.setCancelButton(true);
        saveButton.setDefaultButton(true);
        cancelButton.setOnAction(this::onCancel);
        saveButton.setOnAction(this::onSave);
        buttonBar.getChildren().addAll(cancelButton, saveButton);
        buttonBar.setAlignment(Pos.CENTER_RIGHT);
        buttonBar.setSpacing(10);

        this.setMinWidth(400);
        this.setPadding(new Insets(20));
        this.setSpacing(10);
    }


    public void setOnClose(EditCanceledHandler h) { onCloseHandler = h; }
    public void setOnSave(EditedHandler h) { onSaveHandler = h; }

    void setContents(javafx.scene.Node contents) {
        this.getChildren().addAll(contents, buttonBar);
    }

    private void onCancel(ActionEvent e) {
        if (onCloseHandler == null) return;
        onCloseHandler.handle();
    }

    private void onSave(ActionEvent e) {
        if (onSaveHandler == null) return;
        fillChanges();
        onSaveHandler.handle(getElementId(), changes);
    }

    abstract void fillChanges();
    abstract String getElementId();
}
