package ui;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import main.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class NodeEdit extends Edit {
    private static Callback<ListView<Color>, ListCell<Color>> colorCellFactory = colorListView -> new ColorListCell();
    private static Callback<ListView<String>, ListCell<String>> stickerCellFactory = stickerListView -> new StickerListCell();

    private VBox box = new VBox();
    private NodeData data;

    private HBox toolbar = new HBox();
    private ComboBox<Color> colorDropDown = new ComboBox<>();
    private ComboBox<String> stickerDropDown = new ComboBox<>();

    private Label imageLabel = new Label("Obraz:");
    private Button addImageButton = new Button("Dodaj");
    private Button changeImageButton = new Button("Zmień");
    private Button removeImageButton = new Button("Usuń");

    private TextField label = new TextField();

    {
        ArrayList<Color> cList = new ArrayList<>(Colors.getList());
        cList.add(0, Color.TRANSPARENT);
        colorDropDown.setItems(FXCollections.observableArrayList(cList));
        colorDropDown.setCellFactory(colorCellFactory);
        colorDropDown.setButtonCell(new ColorListCell());

        ArrayList<String> sList = new ArrayList<>(Stickers.getList());
        sList.add(0, ""); // nie null, bug w ComboBox? http://stackoverflow.com/a/25888274
        stickerDropDown.setItems(FXCollections.observableArrayList(sList));
        stickerDropDown.setCellFactory(stickerCellFactory);
        stickerDropDown.setButtonCell(new StickerListCell());

        HBox space = new HBox();
        HBox.setHgrow(space, Priority.ALWAYS);
        addImageButton.setOnAction(this::setImage);
        changeImageButton.setOnAction(this::setImage);
        removeImageButton.setOnAction(this::removeImage);
        toolbar.getChildren().addAll(colorDropDown, stickerDropDown, space, imageLabel);
        toolbar.setSpacing(10);
        toolbar.setAlignment(Pos.BASELINE_CENTER);

        box.setSpacing(10);
        box.getChildren().addAll(toolbar, label);
    }

    public NodeEdit(NodeData data) {
        if (data == null) {
            data = new NodeData() {
                private List<? extends NodeData> list = new ArrayList<>();
                public String getLabel() { return ""; }
                public String getID() { return null; }
                public List<? extends NodeData> getChildren() { return list; }
                public String getImage() { return null; }
            };
        }

        this.data = data;
        if (data.getImage() != null) {
            this.toolbar.getChildren().addAll(changeImageButton, removeImageButton);
        } else {
            this.toolbar.getChildren().addAll(addImageButton);
        }
        this.colorDropDown.setValue(data.getColor() != null ? Colors.get(data.getColor()) : Color.TRANSPARENT);
        this.stickerDropDown.setValue(data.getSticker() != null ? data.getSticker() : "");
        this.label.setText(data.getLabel());

        this.setContents(box);
    }

    @Override
    void fillChanges() {
        changes.label = new DataChange<>(DataChange.EDIT, label.getText());
        Integer sel = colorDropDown.getSelectionModel().getSelectedIndex();
        sel--; if (sel == -1) sel = null;
        changes.color = new DataChange<>(DataChange.EDIT, sel);
        String st = stickerDropDown.getSelectionModel().getSelectedItem();
        String ost = data.getSticker();
        if (ost == null) ost = "";
        if (!ost.equals(st)) changes.sticker = new DataChange<>(DataChange.EDIT, ("".equals(st) ? null : st));
    }

    @Override
    String getElementId() {
        return data.getID();
    }

    private void setImage(ActionEvent e) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Wybierz obraz");
        fileChooser.getExtensionFilters().add(
            new FileChooser.ExtensionFilter("Image", "*.png", "*.jpg"));
        File f = fileChooser.showOpenDialog(null);
        if (f == null) return;

        byte[] content;

        try {
            content = Files.readAllBytes(f.toPath());
        } catch (IOException ex) {
            return;
        }

        changes.image = new DataChange<>(
            DataChange.EDIT,
            Base64.getEncoder().encodeToString(content)
        );

        if (toolbar.getChildren().contains(addImageButton)) {
            toolbar.getChildren().remove(addImageButton);
            toolbar.getChildren().addAll(changeImageButton, removeImageButton);
        }
    }

    private void removeImage(ActionEvent e) {
        changes.image = new DataChange<>(DataChange.REMOVE, null);
        toolbar.getChildren().removeAll(changeImageButton, removeImageButton);
        toolbar.getChildren().addAll(addImageButton);
    }
}
