package ui;

import handlers.*;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import handlers.NodeClickedHandler;
import javafx.geometry.Point2D;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import main.MindMapData;
import main.NodeData;
import SVG.SVG;
import SVG.Tag;
import static java.lang.Math.max;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseButton;
import main.DataChange;
import main.EditDescription;
import main.ManualLayout;

public class MindMap extends ScrollPane {
    private Map<String, Node> nodes = new HashMap<>();
    private List<Edge> edges = new ArrayList<Edge>();
    private Pane p = new Pane();
    private StackPane pad = new StackPane();
    private Root root;
    private Map<String, Point2D> positions;

    private NodeClickedHandler onNodeClicked = null;
    private RootClickedHandler onRootClicked = null;
    private NodeCreateHandler onNodeCreate = null;
    private NodeReattachBeginHandler onNodeReattachBegin = null;
    private NodeRemoveHandler onNodeRemove = null;
    private ManualLayout manualLayout;
    private EditedHandler onNodeEdited;
    private Node nodeToMove;
    private boolean typeOfLayout = false; // false - Automatic, true - Manual 
    
    {
        this.setContent(pad);
        pad.getChildren().add(p);
        pad.setBorder(new Border(new BorderStroke(
            Color.TRANSPARENT,
            BorderStrokeStyle.SOLID,
            null,
            new BorderWidths(50)
        )));
    } 
   
    public void render(final MindMapData data) {
        p.getChildren().clear();
        nodes.clear();
        edges.clear();

        root = new Root(data);

        List<? extends NodeData> topNodes = data.getChildren();
        for (NodeData n : topNodes) createComponents(n);
        
        if (!typeOfLayout)
            positions = data.getLayout().layout(data, root, nodes);
        else
        {
            if (positions == null)
                positions = new HashMap<>();
            if (positions.isEmpty())
            {
                for (String k : nodes.keySet())
                    positions.put(k, nodes.get(k).getPosition());
                setNewRootsPosition(data);
            }
        }
        relocate(root, positions.get(""));
        setRootEvents();
        
        setNodesPositions();
        
        for (String k : nodes.keySet()) {
            Node node = nodes.get(k);
            relocate(node, positions.get(k));
            setNodeEvents(node);

            node.setPosition(positions.get(k));
            node.setOnMouseDragged(this::mouseBeingDragged);
            node.setOnMouseReleased(this::mouseReleased);
        }
        
        createEdges(root, data.getLeftChildren(), true);
        createEdges(root, data.getRightChildren(), false);

        p.getChildren().addAll(edges);
        p.getChildren().addAll(nodes.values());
        p.getChildren().add(root);
    }

    public void scrollToRoot() {
        double scrollh = root.getLayoutX() / p.getBoundsInParent().getWidth();
        double scrollv = root.getLayoutY() / p.getBoundsInParent().getHeight();
        this.setHvalue(scrollh);
        this.setVvalue(scrollv);
    }

    private void createEdges(Node parent, List<? extends NodeData> children, boolean r) {
        for (NodeData x : children) {
            Node cmp = nodes.get(x.getID());
            edges.add(new Edge(parent, cmp, r));
            createEdges(cmp, x.getChildren(), r);
        }
    }

    private void createEdges(Root parent, List<? extends NodeData> children, boolean r) {
        for (NodeData x : children) {
            Node cmp = nodes.get(x.getID());
            edges.add(new Edge(parent, cmp, r));
            createEdges(cmp, x.getChildren(), r);
        }
    }

    private void createComponents(NodeData x) {
        nodes.put(x.getID(), new Node(x));
        for (NodeData n : x.getChildren()) createComponents(n);
    }

    private void relocate(javafx.scene.Node n, Point2D p) {
        n.relocate(Math.round(p.getX()), Math.round(p.getY()));
    }

    private void setNodeEvents(Node node) {
        node.setOnNodeClicked(id -> { if (onNodeClicked != null) onNodeClicked.handle(id); });
        node.setOnNodeCreate(parentId -> { if (onNodeCreate != null) onNodeCreate.handle(parentId); });
        node.setOnNodeReattachBegin(id -> { if (onNodeReattachBegin != null) onNodeReattachBegin.handle(id); });
        node.setOnNodeRemove(id -> { if (onNodeRemove != null) onNodeRemove.handle(id); });
    }

    private void setRootEvents() {
        root.setOnRootClicked(() -> { if (onRootClicked != null) onRootClicked.handle(); });
        root.setOnNodeCreate(parentId -> { if (onNodeCreate != null) onNodeCreate.handle(parentId); });
    }

    public void setOnNodeCreate(NodeCreateHandler h) { onNodeCreate = h; }
    public void setOnNodeReattachBegin(NodeReattachBeginHandler h) { onNodeReattachBegin = h; }
    public void setOnNodeRemove(NodeRemoveHandler h) { onNodeRemove = h; }
    public void setOnNodeClicked(NodeClickedHandler h) { onNodeClicked = h; }
    public void setOnRootClicked(RootClickedHandler h) { onRootClicked = h; }

    public SVG toSVG() {
        Bounds page = p.getBoundsInParent();
        SVG image = new SVG(page.getWidth(), page.getHeight());

        for (Edge e : edges) image.addElement(e.toSVG());
        root.toSVG().forEach(image::addElement);
        for (Node n : nodes.values()) n.toSVG().forEach(image::addElement);

        return image;
    }
    
    public void setOnNodeEdited(EditedHandler h)
    {
        onNodeEdited = h;
    }
    
    public void setTypeOfLayout(boolean typeOfLayout)
    {
        this.typeOfLayout = typeOfLayout;
    }
    
    public boolean getTypeOfLayout()
    {
        return this.typeOfLayout;
    }
    
    private void mouseBeingDragged(MouseEvent e)
    {
        if (!typeOfLayout)
            return;
        if (manualLayout == null)
        {
            manualLayout = new ManualLayout((Node)e.getSource(), this, positions);
            nodeToMove = (Node)e.getSource();
        }
        manualLayout.mouseBeingDragged(e);
    }
    
    private void mouseReleased(MouseEvent e)
    {
        if (!typeOfLayout)
            return;
        if (manualLayout == null || e.getButton() == MouseButton.SECONDARY) // kliknięcie myszką, bez przenoszenia
            return;
        manualLayout.mouseReleased(e);
        manualLayout = null; // nastepne przeniesienie moze byc dla innego wezla
        EditDescription description = new EditDescription();
        description.position = new DataChange<>(DataChange.EDIT, nodeToMove.getPosition());
        onNodeEdited.handle(nodeToMove.getID(), description);
        if (description.position.getContent().getX() < 0 || description.position.getContent().getY() < 0)
            translateMap(Math.max(-description.position.getContent().getX(), 0), Math.max(-description.position.getContent().getY(), 0));
    }
    
    private void setNodesPositions()
    {
        EditDescription description = new EditDescription();
        description.toRender = false;
        for (String k : nodes.keySet())
        {
            if (positions.get(k) != null)
                nodes.get(k).setPosition(positions.get(k));
            else
                positions.put(k, nodes.get(k).getPosition());
            description.position = new DataChange<>(DataChange.EDIT, nodes.get(k).getPosition());
            onNodeEdited.handle(nodes.get(k).getID(), description);
        }
        description.position = new DataChange<>(DataChange.EDIT, positions.get(""));
        onNodeEdited.handle("", description);
    }
    
    public void setNewRootsPosition(MindMapData data)
    {
        if (((model.MindMap)data).getRootsPosition() != null)
            positions.put("", new Point2D(((model.MindMap)data).getRootsPosition().getX(), ((model.MindMap)data).getRootsPosition().getY()));
    }
    
    public void changeLayout(ActionEvent e)
    {
        if (((ComboBox)e.getSource()).getValue().equals("Rozkład automatyczny"))
            this.typeOfLayout = false;
        else
            this.typeOfLayout = true;
        EditDescription description = new EditDescription();
        description.layout = typeOfLayout;
        onNodeEdited.handle("-1", description);
    }
    
    private void translateMap(double x, double y)
    {
        Point2D vector = new Point2D(x, y);
        for (String k : nodes.keySet())
        {
            positions.put(k, positions.get(k).add(vector));
        }
        positions.put("", positions.get("").add(vector));
        onNodeEdited.handle("-1", new EditDescription());
    }
}
