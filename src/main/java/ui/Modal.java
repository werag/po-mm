package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class Modal extends VBox {
    final static Background overlay = new Background(
        new BackgroundFill(Color.BLACK.deriveColor(0, 1, 1, 0.5), null, null));

    final static Background fill = new Background(
        new BackgroundFill(Color.web("#eeeeee"), null, null));

    final static double margin = 20;

    private Pane inside = new Pane();

    public Modal() {
        this.setVisible(false);
        this.setBackground(overlay);
        this.setAlignment(Pos.CENTER);
        this.setFillWidth(false);
        this.setPadding(new Insets(margin));

        inside.setBackground(fill);

        this.getChildren().add(inside);
    }

    public void setContent(javafx.scene.Node n) {
        inside.getChildren().add(n);
    }

    public void show() {
        this.setVisible(true);
    }

    public void close() {
        this.setVisible(false);
        inside.getChildren().clear();
    }
}
