package ui;

import SVG.SVG;
import SVG.Tag;
import handlers.*;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import main.Colors;
import main.NodeData;
import main.Stickers;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class Node extends VBox {
    static final Font fontRegular = Font.loadFont(MindMap.class.getResourceAsStream("SourceSansPro-Regular.ttf"), 15);

    static final double padding = 8;
    static final double spacing = 5;

    private NodeData data;
    private Text label;
    private Image imageContent;
    private ImageView image;
    private ImageView sticker;
    private HBox labelBox;
    private ContextMenu contextMenu;

    private NodeCreateHandler onNodeCreate = null;
    private NodeReattachBeginHandler onNodeReattachBegin = null;
    private NodeRemoveHandler onNodeRemove = null;
    private NodeClickedHandler onNodeClicked = null;
    private Point2D position;
    private String ID;

    private void changeColor(Integer color) {
        Color c = Colors.get(color);
        Background bg = new Background(new BackgroundFill(c, null, null));
        this.label.setFill(c.deriveColor(0, 0.2, 3, 1));
        labelBox.setBackground(bg);
    }
    
    public Node(NodeData data) {
        this.data = data;
        this.ID = data.getID();
        this.setSpacing(spacing);
        this.setFillWidth(false);
        if (((model.Node)data).getPosition() != null)
            this.position = ((model.Node)data).getPosition();
        else
            this.position = new Point2D(200, 200);
        this.label = new Text(data.getLabel());
        this.label.setFont(fontRegular);
        this.labelBox = new HBox(this.label);
        labelBox.setPadding(new Insets(padding));
        labelBox.setSpacing(10);
        labelBox.setAlignment(Pos.CENTER_LEFT);

        if (data.getImage() != null) {
            ByteArrayInputStream is = new ByteArrayInputStream(DatatypeConverter.parseBase64Binary(data.getImage()));
            this.imageContent = new Image(is);
            this.image = new ImageView(imageContent);
            this.getChildren().add(this.image);
        }

        if (data.getSticker() != null) {
            Image img = Stickers.getImage(data.getSticker());
            if (img != null) {
                sticker = new ImageView(img);
                labelBox.getChildren().add(0, sticker);
            }
        }

        this.changeColor(data.getVisibleColor());
        this.createContextMenu();
        this.setOnMouseClicked(this::onClick);
        this.getChildren().add(labelBox);

        Scene scene = new Scene(new Pane(this), 0, 0);
        scene.snapshot(null);
    }

    public double getFullWidth() {
        return this.getBoundsInParent().getWidth();
    }
    public double getFullHeight() {
        return this.getBoundsInParent().getHeight();
    }

    public Point2D getInPoint() {
        return new Point2D(0, getFullHeight() / 2);
    }

    public Point2D getOutPoint() {
        return new Point2D(getFullWidth(), getFullHeight() / 2);
    }

    public NodeData getData() {
        return this.data;
    }

    private void onClick(MouseEvent e) {
        if (e.getButton() == MouseButton.SECONDARY) {
            displayContextMenu(e.getScreenX(), e.getScreenY());
        } else if (e.getButton() == MouseButton.PRIMARY) {
            if (onNodeClicked != null) onNodeClicked.handle(data.getID());
        }
    }

    private void createContextMenu () {
        contextMenu = new ContextMenu();

        MenuItem addChild = new MenuItem("Dodaj podpunkt");
        addChild.setOnAction(actionEvent -> {
            contextMenu.hide();
            if (onNodeCreate != null) onNodeCreate.handle(data.getID());
        });

        MenuItem reattach = new MenuItem("Przepnij");
        reattach.setOnAction(actionEvent -> {
            contextMenu.hide();
            if (onNodeReattachBegin != null) onNodeReattachBegin.handle(data.getID());
        });

        MenuItem remove = new MenuItem("Usuń");
        remove.setOnAction(actionEvent -> {
            contextMenu.hide();
            if (onNodeRemove != null) onNodeRemove.handle(data.getID());
        });

        contextMenu.getItems().addAll(addChild, reattach, remove);
    }

    private void displayContextMenu(double x, double y) {
        contextMenu.show(this, x, y);
    }

    public void setOnNodeCreate(NodeCreateHandler h) { onNodeCreate = h; }
    public void setOnNodeReattachBegin(NodeReattachBeginHandler h) { onNodeReattachBegin = h; }
    public void setOnNodeRemove(NodeRemoveHandler h) { onNodeRemove = h; }
    public void setOnNodeClicked(NodeClickedHandler h) { onNodeClicked = h; }

    public List<Tag> toSVG() {
        List<Tag> res = new ArrayList<>();
        Bounds b = getBoundsInParent();
        Bounds lbb = labelBox.getBoundsInParent();
        Bounds lb = label.getBoundsInParent();

        Point2D lbp = new Point2D(b.getMinX(), b.getMinY());
        lbp = lbp.add(lbb.getMinX(), lbb.getMinY());
        Point2D lp = lbp.add(lb.getMinX(), lb.getMinY() + 13);

        res.add(SVG.makeRectangle(
            lbp.getX(), lbp.getY(),
            lbb.getWidth(), lbb.getHeight(),
            Colors.getBackground(labelBox)
        ));

        if (data.getImage() != null) {
            res.add(SVG.makeImage(
                b.getMinX(), b.getMinY(),
                imageContent.getWidth(), imageContent.getHeight(),
                data.getImage()
            ));
        }

        if (sticker != null) {
            String st = Stickers.getBase64(data.getSticker());
            Bounds sb = sticker.getBoundsInParent();
            res.add(SVG.makeImage(
                b.getMinX() + sb.getMinX(), b.getMinY() + sb.getMinY(),
                sb.getWidth(), sb.getHeight(),
                st
            ));
        }

        res.add(SVG.makeText(lp.getX(), lp.getY()+2, label.getText(), (Color) label.getFill()));
        return res;
    }
    
    public Point2D getPosition()
    {
        return this.position;
    }
    
    public void setPosition(Point2D newPosition)
    {
        this.position = newPosition;
    }
    
    public String getID()
    {
        return this.ID;
    }
}
