package ui;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import main.Stickers;

public class StickerListCell extends ListCell<String> {
    private static ColorAdjust dark = new ColorAdjust();

    static {
        dark.setBrightness(-0.8);
    }
    
    private final ImageView icon;

    {
        setContentDisplay(ContentDisplay.LEFT);
        icon = new ImageView();
        icon.setEffect(dark);
    }

    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || item.equals("") || empty) {
            setGraphic(null);
            setText("Brak ikony");
        } else {
            icon.setImage(Stickers.getImage(item));
            setGraphic(icon);
            setText(Stickers.getDisplayName(item));
        }

    }
}