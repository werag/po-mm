package ui;

import SVG.SVG;
import SVG.Tag;
import handlers.NodeCreateHandler;
import handlers.RootClickedHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import main.MindMapData;

import java.util.ArrayList;
import java.util.List;

public class Root extends VBox {
    static final Font fontBold = Font.loadFont(MindMap.class.getResourceAsStream("SourceSansPro-Bold.ttf"), 15);
    static final double padding = 8;

    static final Color bgcolor = Color.web("#333");
    static final Color fcolor = Color.web("#eee");
    static final Background bg = new Background(new BackgroundFill(bgcolor, null, null));

    private Label title;
    private ContextMenu contextMenu;

    private RootClickedHandler onRootClicked = null;
    private NodeCreateHandler onNodeCreate = null;

    public Root(MindMapData data) {

        title=new Label(data.getTitle());
        title.setAlignment(Pos.CENTER);
        title.setMaxWidth(200);
        title.setWrapText(true);
        title.setFont(fontBold);
        title.setTextFill(fcolor);
        title.setTextAlignment(TextAlignment.CENTER);

        this.setBackground(bg);
        this.setPadding(new Insets(padding));
        this.setOnMouseClicked(this::onClick);
        this.createContextMenu();

        this.getChildren().add(title);

        Scene scene = new Scene(new Pane(this), 0, 0);
        scene.snapshot(null);
    }

    public void setOnRootClicked(RootClickedHandler h) {
        onRootClicked = h;
    }
    public void setOnNodeCreate(NodeCreateHandler h) { onNodeCreate = h; }

    private void onClick(MouseEvent e){
        if (e.getButton() == MouseButton.SECONDARY) {
            displayContextMenu(e.getScreenX(), e.getScreenY());
        } else if (e.getButton() == MouseButton.PRIMARY) {
            if (onRootClicked != null) onRootClicked.handle();
        }
    }

    public double getFullWidth() {
        return this.getBoundsInParent().getWidth();
    }
    public double getFullHeight() {
        return this.getBoundsInParent().getHeight();
    }
    public Point2D getOutPoint() { return new Point2D(this.getFullWidth() / 2, this.getFullHeight() / 2); }

    private void createContextMenu () {
        contextMenu = new ContextMenu();

        MenuItem addChild = new MenuItem("Dodaj podpunkt");
        addChild.setOnAction(actionEvent -> {
            contextMenu.hide();
            if (onNodeCreate != null) onNodeCreate.handle("");
        });

        contextMenu.getItems().addAll(addChild);
    }

    private void displayContextMenu(double x, double y) {
        contextMenu.show(this, x, y);
    }

    public List<Tag> toSVG() {
        List<Tag> res = new ArrayList<>();
        Bounds b = getBoundsInParent();
        Bounds tb = title.getBoundsInParent();
        res.add(SVG.makeRectangle(b.getMinX(), b.getMinY(), b.getWidth(), b.getHeight(), bgcolor));
        res.add(SVG.makeText(b.getMinX() + tb.getMinX(), b.getMinY() + tb.getMinY() + 15, title.getText(), fcolor, true));
        return res;
    }
}
