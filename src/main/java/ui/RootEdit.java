package ui;

import javafx.scene.control.TextField;
import main.DataChange;
import main.MindMapData;

public class RootEdit extends Edit {
    private MindMapData data;
    private TextField label = new TextField();

    public RootEdit(String title) {
        label.setText(title);
        this.setContents(label);
    }

    @Override
    void fillChanges() {
        changes.label = new DataChange<>(DataChange.EDIT, label.getText());
    }

    @Override
    String getElementId() { return ""; }
}
