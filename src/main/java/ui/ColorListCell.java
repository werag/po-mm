package ui;

import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import main.Colors;

public class ColorListCell extends ListCell<Color> {
    private final Rectangle rectangle;

    {
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        rectangle = new Rectangle(15, 15);
    }

    @Override
    protected void updateItem(Color item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            setGraphic(null);
        } else {
            rectangle.setFill(item);
            rectangle.setStroke(Color.BLACK);
            setGraphic(rectangle);
        }

    }
}
