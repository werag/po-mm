package ui;

import SVG.Attr;
import SVG.Tag;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import main.Colors;

public class Edge extends Line {
    static final double lineWidth = 2;

    private Root r;
    private Node a;
    private Node b;
    private boolean reverse;

    private void link() {
        Point2D start = getStart();
        Point2D end = getEnd();
        setStart(start);
        setEnd(end);

        this.setStroke(Colors.get(b.getData().getVisibleColor()));
        this.setStrokeWidth(lineWidth);
    };

    public Edge(Node b, boolean reverse) {
        this.b = b;
        this.reverse = reverse;
    }

    public Edge(Node a, Node b, boolean reverse) {
        this(b, reverse);
        this.a = a;
        link();
    }

    public Edge(Root a, Node b, boolean reverse) {
        this(b, reverse);
        this.r = a;
        link();
    }

    public Edge(Node a, Node b) { this(a, b, false); }

    private void setStart(Point2D x) {
        this.setStartX(x.getX());
        this.setStartY(x.getY());
    }

    private void setEnd(Point2D x) {
        this.setEndX(x.getX());
        this.setEndY(x.getY());
    }

    private Point2D getStartR() {
        Point2D rel = new Point2D(this.r.getLayoutX(), this.r.getLayoutY());
        return this.r.getOutPoint().add(rel);
    }

    private Point2D getStart() {
        if (a == null) return getStartR();
        Point2D rel = new Point2D(this.a.getLayoutX(), this.a.getLayoutY());
        if (!reverse)
            return this.a.getOutPoint().add(rel);
        else
            return this.a.getInPoint().add(rel);
    }

    private Point2D getEnd() {
        Point2D rel = new Point2D(this.b.getLayoutX(), this.b.getLayoutY());
        if (!reverse)
            return this.b.getInPoint().add(rel);
        else
            return this.b.getOutPoint().add(rel);
    }

    public Tag toSVG() {
        Tag res = new Tag("line");
        res.add(new Attr("x1", getStart().getX()));
        res.add(new Attr("y1", getStart().getY()));
        res.add(new Attr("x2", getEnd().getX()));
        res.add(new Attr("y2", getEnd().getY()));
        res.add(new Attr("stroke", Colors.toHex((Color) this.getStroke())));
        res.add(new Attr("stroke-width", lineWidth));
        return res;
    }
}
