package ui;

import handlers.*;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.ComboBox;

public class Toolbar extends ToolBar {
    private Button buttonNew    = new Button("Nowy");
    private Button buttonOpen   = new Button("Otwórz");
    private Button buttonSave   = new Button("Zapisz");
    private Button buttonSaveAs = new Button("Zapisz jako");
    private Button buttonExport = new Button("Eksport");
    private ComboBox<String> layoutButton = new ComboBox<>(FXCollections.observableArrayList(listOfLayouts()));

    private Runnable onNewMap = null;
    private PreOpenMapHandler onOpenMap = null;
    private PreSaveMapHandler onSaveMap = null;
    private PreExportMapHandler onExportMap = null;
    private ChangeLayoutHandler onLayoutChange = null;

    public Toolbar() {
        super();
        this.getItems().addAll(buttonNew, buttonOpen, buttonSave, buttonSaveAs, buttonExport, layoutButton);

        buttonNew.setOnMouseClicked((MouseEvent e) -> {
            if (onNewMap != null) onNewMap.run();
        });

        buttonOpen.setOnMouseClicked((MouseEvent e) -> {
            if (onOpenMap != null) onOpenMap.handle();
        });

        buttonSave.setOnMouseClicked((MouseEvent e) -> {
            if (onSaveMap != null) onSaveMap.handle(false);
        });

        buttonSaveAs.setOnMouseClicked((MouseEvent e) -> {
            if (onSaveMap != null) onSaveMap.handle(true);
        });

        buttonExport.setOnMouseClicked(e -> {
            if (onExportMap != null) onExportMap.handle();
        });
        
        layoutButton.setValue("Rozkład automatyczny");
        layoutButton.setOnAction(e -> 
        {
            onLayoutChange.handle(e);
        });
    }

    public void setOnNewMap(Runnable h) { onNewMap = h; }
    public void setOnOpenMap(PreOpenMapHandler h) { onOpenMap = h; }
    public void setOnSaveMap(PreSaveMapHandler h) { onSaveMap = h; }
    public void setOnExportMap(PreExportMapHandler h) { onExportMap = h; }
    public void setOnLayoutChange(ChangeLayoutHandler h)
    {
        onLayoutChange = h;
    }
    public void changeLayoutButton()
    {
        if (layoutButton.getValue().equals("Rozkład automatyczny"))
            layoutButton.setValue("Rozkład ręczny");
        else
            layoutButton.setValue("Rozkład automatyczny");
    }
    
    private ArrayList<String> listOfLayouts()
    {
        ArrayList<String> listOfLayouts = new ArrayList<>();
        listOfLayouts.add("Rozkład ręczny");
        listOfLayouts.add("Rozkład automatyczny");
        return listOfLayouts;
    }
}
