package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class RemoveConfirm extends VBox {
    private HBox buttonBar;
    private Button yesButton = new Button("Tak");
    private Button noButton = new Button("Nie");

    private Runnable onYes;
    private Runnable onNo;

    {
        this.setPadding(new Insets(20));
        this.setSpacing(10);

        buttonBar = new HBox(noButton, yesButton);
        buttonBar.setAlignment(Pos.CENTER_RIGHT);
        buttonBar.setSpacing(10);

        yesButton.setDefaultButton(true);
        noButton.setCancelButton(true);

        yesButton.setOnAction(actionEvent -> { if (onYes != null) onYes.run(); });
        noButton.setOnAction(actionEvent -> { if (onNo != null) onNo.run(); });
    }

    public RemoveConfirm(String text, Runnable onYes, Runnable onNo) {
        this.onYes = onYes;
        this.onNo = onNo;
        this.getChildren().addAll(new Text(text), buttonBar);
    }
}
