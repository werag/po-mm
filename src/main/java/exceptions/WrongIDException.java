package exceptions;

public class WrongIDException extends Throwable{
    String id;
    public WrongIDException(String id){
        this.id=id;
    }
    public String toString(){
        return "Wierzchołek o ID "+id+" nie istnieje !";
    }
}


