package main;

public class DataChange<T> {
    private T content;
    private int type;

    public static final int REMOVE = 0;
    public static final int EDIT = 1;

    public DataChange(int type, T content) {
        this.type = type;
        this.content = content;
    }

    public T getContent() {
        return this.content;
    }

    public int getType() {
        return this.type;
    }
}
