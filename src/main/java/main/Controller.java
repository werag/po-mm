package main;

import model.Model;
import org.json.simple.parser.ParseException;

import java.io.FileWriter;
import java.io.IOException;

public class Controller {
    Model model;
    View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;

        model.requestUpdates(view);

        view.setOnNewMap(() -> this.handleClose(model::newMindMap));

        view.setOnOpenMap(path -> this.handleClose(() -> {
            try {
                model.loadFile(path);
            } catch (ParseException | IOException e) {
                e.printStackTrace();
            }
        }));

        view.setOnSaveMap(path -> {
            try {
                model.saveFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        view.setOnExportMap((path, content) -> {
            try {
                FileWriter file = new FileWriter(path);
                file.write(content);
                file.flush();
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        view.setOnMapRenamed(model::renameMindMap);

        view.setOnNodeEdited(model::editNode);
        view.setOnNodeCreated(model::addNode);
        view.setOnNodeReattached(model::reattachNode);
        view.setOnNodeRemove(model::removeNode);

        view.setOnCloseProgram(event -> this.handleClose(view::close, event::consume));
    }

    private void handleClose(Runnable pass) {
        handleClose(pass, ignore);
    }

    private void handleClose(Runnable pass, Runnable stop) {
        if (model.isDirty()) {
            view.showUnsavedCloseConfirm(pass, stop);
        } else {
            pass.run();
        }
    }

    private Runnable ignore = () -> {};
}
