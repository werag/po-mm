package main;

import javafx.geometry.Point2D;

public class EditDescription {
    public DataChange<String> label = null;
    public DataChange<String> image = null;
    public DataChange<Integer> color = null;
    public DataChange<String> sticker = null;
    public DataChange<Point2D> position = null;
    public boolean toRender = true;
    public Boolean layout = null;
}
