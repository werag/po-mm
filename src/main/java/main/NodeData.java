package main;

import java.util.List;

public interface NodeData {
    String getLabel();
    String getID();
    List<? extends NodeData> getChildren();

    String getImage();
    default Integer getColor() { return null; }
    default Integer getVisibleColor() { return null; }
    default String getSticker() { return null; }
}
