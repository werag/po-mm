package main;

import java.io.IOException;
import javafx.application.Application;
import javafx.stage.Stage;
import model.Model;

import java.util.List;
import org.json.simple.parser.ParseException;

public class MindMapEditor extends Application {
    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException, ParseException {
        List<String> args = this.getParameters().getRaw();
        Model m = new Model();
        View v = new View(stage);
        Controller c = new Controller(m, v);
        
        if (args.size() > 0) {
            m.loadFile(args.get(0));
        } else {
            m.newMindMap();
        }
    }
}
