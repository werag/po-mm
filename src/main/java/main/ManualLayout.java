package main;

import java.util.Map;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import ui.Node;
import ui.Root;
import ui.MindMap;

public class ManualLayout implements MindMapLayout
{
    private Integer previousX;
    private Integer previousY;
    private Integer currentX;
    private Integer currentY;
    private Integer primaryX;
    private Integer primaryY;
    private final Node nodeToMove;
    private final MindMap mindMap;
    private Map<String, Point2D> positions;
    private int deltaX;
    private int deltaY;
    
    public ManualLayout(Node nodeToMove, MindMap mindMap, Map<String, Point2D> positions)
    {
        this.nodeToMove = nodeToMove;
        this.mindMap = mindMap;
        this.positions = positions;
    }

    @Override
    public Map<String, Point2D> layout(MindMapData data, Root rootComponent, Map<String, Node> nodeComponents)
    {
        return this.positions;
    }
    
    public void mouseBeingDragged(MouseEvent e)
    {
        if (previousX == null)
        {
            previousX = (int) e.getScreenX();
            previousY = (int) e.getScreenY();
            primaryX = (int) e.getScreenX();
            primaryY = (int) e.getScreenY();
            return;
        }
        currentX = (int) e.getScreenX();
        currentY = (int) e.getScreenY();
        
        deltaX = currentX - previousX;
        deltaY = currentY - previousY;
        
        previousX = currentX;
        previousY = currentY;
        
        nodeToMove.setTranslateX(nodeToMove.getTranslateX() + deltaX);
        nodeToMove.setTranslateY(nodeToMove.getTranslateY() + deltaY);
    }
    
    public void mouseReleased(MouseEvent e)
    {
        String id = ((model.Node) nodeToMove.getData()).getID();
        Point2D position = new Point2D(positions.get(id).getX(), positions.get(id).getY());
        positions.remove(id);
        positions.put(id, position.add(currentX - primaryX, currentY - primaryY));
        nodeToMove.setPosition(positions.get(id));
    }
}
