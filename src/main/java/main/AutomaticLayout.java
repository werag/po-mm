package main;

import javafx.geometry.Point2D;
import ui.Edge;
import ui.Node;
import ui.Root;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutomaticLayout implements MindMapLayout {
    private Map<String, Point2D> result = new HashMap<>();
    private Map<String, Point2D> relative = new HashMap<>();

    private Root root;
    private Map<String, Node> nodes;

    final static double vspace = 20;
    final static double hspace = 30;

    @Override
    public Map<String, Point2D> layout(MindMapData data, Root root, Map<String, Node> nodes) {
        result.clear();
        relative.clear();

        this.root = root;
        this.nodes = nodes;

        layoutNode(false, data.getRightChildren(), root.getFullHeight(), root.getFullWidth());
        layoutNode(true, data.getLeftChildren(), root.getFullHeight(), root.getFullWidth());

        relative.put("", new Point2D(0, 0));
        result.put("", new Point2D(0, 0));

        makeAbsolute(new Point2D(0, 0), data.getChildren());

        Point2D topLeft = new Point2D(0, 0);

        for (Point2D x : result.values()) {
            topLeft = new Point2D(Double.min(topLeft.getX(), x.getX()), Double.min(topLeft.getY(), x.getY()));
        }

        relative.clear();
        relative.putAll(result);
        result.clear();

        topLeft = topLeft.multiply(-1);

        for (String k : relative.keySet()) {
            result.put(k, relative.get(k).add(topLeft));
        }

        return result;
    }

    private Double layoutNode(boolean left, List<? extends NodeData> children, Double h, Double w) {
        if (children == null) return h;

        List<Double> heights = new ArrayList<>();
        double sum = 0d;

        for (NodeData y : children) {
            Node cmp = nodes.get(y.getID());
            heights.add(layoutNode(
                left,
                y.getChildren(),
                cmp.getFullHeight(),
                cmp.getFullWidth()));
            sum += heights.get(heights.size()-1);
        }

        sum += vspace * (heights.size()-1);
        Point2D rel = new Point2D(left ? -hspace : w + hspace, -(sum - h) / 2);
        double top = 0;

        for (int i = 0; i < children.size(); i++) {
            String id = children.get(i).getID();
            Point2D pos = rel.add(
                left ? -nodes.get(id).getFullWidth() : 0,
                top + (heights.get(i) - nodes.get(id).getFullHeight()) / 2
            );

            relative.put(id, pos);
            top += heights.get(i) + vspace;
        }

        return Double.max(sum, h);
    }

    private void makeAbsolute(Point2D top, List<? extends NodeData> children) {
        for (NodeData x : children) {
            String id = x.getID();
            result.put(id, relative.get(id).add(top));
            makeAbsolute(result.get(id), x.getChildren());
        }
    }
}
