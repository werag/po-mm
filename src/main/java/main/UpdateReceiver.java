package main;

public interface UpdateReceiver {
    public void pushUpdate(MindMapData data);
}
