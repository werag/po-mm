package main;

import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Random;

public class Colors {
    private static Integer defaultColor = 0;
    private static ArrayList<Color> list = new ArrayList<>();

    private static Random rg = new Random();

    static {
        list.add(Color.web("#377790"));
        list.add(Color.web("#418D35"));
        list.add(Color.web("#779834"));
        list.add(Color.web("#947433"));
        list.add(Color.web("#984034"));
        list.add(Color.web("#564681"));
    }

    public static Color get(Integer i) {
        return list.get(i != null ? i % list.size() : defaultColor);
    }

    public static ArrayList<Color> getList() { return list; }

    public static Integer random() {
        return rg.nextInt(list.size());
    }

    public static String toHex(Color c) {
        return String.format("#%02X%02X%02X",
            (int)(c.getRed() * 255), (int) (c.getGreen() * 255), (int) (c.getBlue() * 255)
        );
    }

    public static Color getBackground(Region n) {
        return (Color) n.getBackground().getFills().get(0).getFill();
    }
}
