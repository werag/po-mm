package main;

import handlers.*;
import java.io.File;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import ui.*;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;

public class View implements UpdateReceiver {
    private static final KeyCombination keyNewFile = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN);
    private static final KeyCombination keyOpenFile = new KeyCodeCombination(KeyCode.O, KeyCombination.CONTROL_DOWN);
    private static final KeyCombination keySaveFile = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
    private static final KeyCombination keySaveFileAs = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);

    private interface SelectionAction {
        void run(String id);
    }

    private VBox window = new VBox();
    private Toolbar toolbar = new Toolbar();
    private Modal modal = new Modal();
    private MindMap mindMapPane = new MindMap();
    private NodeEdit nodeEditor;

    private MindMapData data = null;
    private Map<String, NodeData> nodes = new HashMap<>();
    private String currentPath = null;

    private Runnable onNewMap = null;
    private OpenMapHandler onOpenMap = null;
    private SaveMapHandler onSaveMap = null;
    private ExportMapHandler onExportMap = null;
    private CloseProgramHandler onCloseProgram = null;

    private EditedHandler onNodeEdited = null;
    private NodeCreatedHandler onNodeCreated = null;
    private NodeReattachedHandler onNodeReattached = null;
    private NodeRemoveHandler onNodeRemove = null;
    private MapRenamedHandler onMapRenamed = null;

    private Boolean selectionMode = false;
    private SelectionAction selectionAction = null;

    private Stage stage;

    public View(Stage stage) {
        Scene scene = new Scene(window, 500, 500);

        stage.setTitle("Mind Map Editor");
        stage.setScene(scene);
        stage.setOnCloseRequest(event -> {
            if (this.onCloseProgram != null) {
                event.consume();
                this.onCloseProgram.handle(event);
            }
        });
        stage.show();
        this.stage = stage;

        StackPane inside = new StackPane(mindMapPane, modal);
        VBox.setVgrow(inside, Priority.ALWAYS);

        window.getChildren().addAll(toolbar, inside);

        toolbar.setOnNewMap(() -> { if (onNewMap == null) return; onNewMap.run(); });
        toolbar.setOnOpenMap(this::openMap);
        toolbar.setOnSaveMap(this::saveMap);
        toolbar.setOnExportMap(this::exportMap);
        toolbar.setOnLayoutChange(mindMapPane::changeLayout);

        MainKeyEventHandler keyHandler = new MainKeyEventHandler();
        keyHandler.addHotkey(keyNewFile, () -> {});
        keyHandler.addHotkey(keyOpenFile, this::openMap);
        keyHandler.addHotkey(keySaveFile, () -> saveMap(false));
        keyHandler.addHotkey(keySaveFileAs, () -> saveMap(true));
        scene.addEventHandler(KeyEvent.KEY_RELEASED, keyHandler);

        mindMapPane.setOnNodeClicked(this::handleNodeClicked);
        mindMapPane.setOnRootClicked(this::handleRootClicked);

        mindMapPane.setOnNodeCreate(parentId -> showNodeEdit(
            null,
            (String id2, EditDescription description) -> {
                if (!parentId.equals(""))
                    description.position = new DataChange<>(DataChange.EDIT, ((model.Node)nodes.get(parentId)).getPosition().add(40, 40));
                else
                    description.position = new DataChange<>(DataChange.EDIT, ((model.MindMap)this.data).getRootsPosition().add(40, 40));
                if (onNodeCreated != null) onNodeCreated.handle(parentId, description);
                modal.close();
            }
        ));

        mindMapPane.setOnNodeReattachBegin(this::handleNodeReattachBegin);

        mindMapPane.setOnNodeRemove(id -> {
            if (nodes.get(id).getChildren().size() > 0) {
                showRemoveConfirm(() -> {
                    if (onNodeRemove != null) onNodeRemove.handle(id);
                });
            } else {
                if (onNodeRemove != null) onNodeRemove.handle(id);
            }
        });
    }

    @Override
    public void pushUpdate(final MindMapData data) {
        
        currentPath = data.getFilePath();
        for (NodeData x : data.getChildren()) {
            indexSubnodes(x);
        }

        boolean wasDifferent = this.data == null || this.data != data;
        if (wasDifferent)
        {
            this.data = data;
            mindMapPane.setNewRootsPosition(data);
            if (mindMapPane.getTypeOfLayout() != ((model.MindMap)data).getTypeOfLayout())
            {
                mindMapPane.setTypeOfLayout(((model.MindMap)data).getTypeOfLayout());
                toolbar.changeLayoutButton();
            }
        }
        else
            ((model.MindMap)data).setTypeOfLayout(mindMapPane.getTypeOfLayout());

        mindMapPane.render(data);
        if (wasDifferent) mindMapPane.scrollToRoot();
    }

    private void indexSubnodes(NodeData n) {
        nodes.put(n.getID(), n);

        for (NodeData x : n.getChildren()) {
            indexSubnodes(x);
        }
    }

    public void setOnNodeEdited(EditedHandler h) 
    {
        onNodeEdited = h;
        mindMapPane.setOnNodeEdited(h);
    }
    public void setOnNodeCreated(NodeCreatedHandler h) { onNodeCreated = h; }
    public void setOnNodeReattached(NodeReattachedHandler h) { onNodeReattached = h; }
    public void setOnNodeRemove(NodeRemoveHandler h) { onNodeRemove = h; }
    public void setOnMapRenamed(MapRenamedHandler h) { onMapRenamed = h; }
    public void setOnCloseProgram(CloseProgramHandler h) { onCloseProgram = h; }
    
    public void setOnNewMap(Runnable h) { onNewMap = h; }
    public void setOnOpenMap(OpenMapHandler h) { onOpenMap = h; }
    public void setOnSaveMap(SaveMapHandler h) { onSaveMap = h; }
    public void setOnExportMap(ExportMapHandler h) { onExportMap = h; }

    public String exportString() {
        return mindMapPane.toSVG().toString();
    }

    private void saveMap(boolean getPath) {
        if (onSaveMap == null) return;

        String path = null;
        if (getPath || currentPath == null) {
            path = getPathToSave();
            if (path == null) return;
        }

        onSaveMap.handle(path);
    }

    private void openMap() {
        if (onOpenMap == null) return;
        String path = getPathToOpen();
        if (path == null) return;
        onOpenMap.handle(path);
    }

    private void exportMap() {
        if (onExportMap == null) return;
        String path = getPathToExport();
        if (path == null) return;
        String content = exportString();
        onExportMap.handle(path, content);
    }

    private void showNodeEdit(NodeData data, EditedHandler handler) {
        NodeEdit nodeEditor = new NodeEdit(data);
        nodeEditor.setOnClose(() -> modal.close());
        nodeEditor.setOnSave(handler);

        modal.setContent(nodeEditor);
        modal.show();
    }

    private void showRemoveConfirm(Runnable onConfirm) {
        RemoveConfirm confirm = new RemoveConfirm(
            "Czy na pewno usunąć całe poddrzewo?",
            () -> {
                modal.close();
                onConfirm.run();
            },
            () -> modal.close()
        );

        modal.setContent(confirm);
        modal.show();
    }

    public void showUnsavedCloseConfirm(Runnable onYes, Runnable onNo) {
        RemoveConfirm confirm = new RemoveConfirm(
            "Nie zapisano zmian! Czy na pewno wyjść z pliku?",
            () -> {
                modal.close();
                onYes.run();
            }, () -> {
                modal.close();
                onNo.run();
            }
        );

        modal.setContent(confirm);
        modal.show();
    }

    public void close() {
        stage.close();
    }

    private void showRootEdit() {
        RootEdit rootEditor = new RootEdit(data.getTitle());
        rootEditor.setOnClose(() -> modal.close());
        rootEditor.setOnSave(this::handleRootEdited);

        modal.setContent(rootEditor);
        modal.show();
    }

    private void handleNodeClicked(String id) {
        if (!selectionMode) {
            showNodeEdit(
                nodes.get(id),
                (String id2, EditDescription description) -> {
                    if (onNodeEdited != null) onNodeEdited.handle(id, description);
                    modal.close();
                }
            );
        } else {
            handleSelection(id);
        }
    }

    private void handleRootClicked() {
        if (!selectionMode) {
            showRootEdit();
        } else {
            handleSelection("");
        }
    }

    private void handleNodeReattachBegin(String id) {
        selectionMode = true;
        selectionAction = newParentId -> {
            if (onNodeReattached != null) {
                onNodeReattached.handle(id, newParentId, null);
            }
            selectionMode = false;
        };
    }

    private void handleSelection(String id) {
        if (selectionAction != null) {
            selectionAction.run(id);
        }
    }

    private void handleRootEdited(String id, EditDescription description) {
        modal.close();
        if (onMapRenamed != null) onMapRenamed.handle(description.label.getContent());
    }

    private String getPathToOpen() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Otwórz mapę myśli");
        fileChooser.getExtensionFilters().add(
            new FileChooser.ExtensionFilter("Mind Map", "*.mmap"));
        File f = fileChooser.showOpenDialog(null);
        return f == null ? null : f.getAbsolutePath();
    }

    private String getPathToSave() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Zapisz plik");
        fileChooser.getExtensionFilters().add(
            new FileChooser.ExtensionFilter("Mind Map", "*.mmap"));
        File f = fileChooser.showSaveDialog(null);
        return f == null ? null : appendExtension(f.getAbsolutePath(), "mmap");
    }

    private String getPathToExport() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Zapisz obraz");
        fileChooser.getExtensionFilters().add(
            new FileChooser.ExtensionFilter("SVG", "*.svg"));
        File f = fileChooser.showSaveDialog(null);
        return f == null ? null : appendExtension(f.getAbsolutePath(), "svg");
    }

    private String appendExtension(String path, String ext) {
        int ix = path.length() - ext.length() - 1;
        if (!path.substring(Integer.max(0, ix)).equals("." + ext)) {
            return path + "." + ext;
        } else {
            return path;
        }
    }
}
