package main;

import javafx.event.EventHandler;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.util.Pair;
import java.util.ArrayList;

public class MainKeyEventHandler implements EventHandler<KeyEvent> {
    private ArrayList<Pair<KeyCombination, Runnable>> combinations = new ArrayList<>();

    @Override
    public void handle(KeyEvent e) {
        for (Pair<KeyCombination, Runnable> p : combinations) {
            if (p.getKey().match(e)) {
                p.getValue().run();
            }
        }
    }

    public void addHotkey(KeyCombination c, Runnable r) {
        combinations.add(new Pair<>(c, r));
    }
}
