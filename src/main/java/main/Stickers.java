package main;

import javafx.scene.image.Image;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.*;

/*
 * Entypo pictograms by Daniel Bruce — www.entypo.com
 */

public class Stickers {
    private static ArrayList<String> names = new ArrayList<>();
    private static Map<String, Image> images = new HashMap<>();
    private static Map<String, String> displayNames = new HashMap<>();
    private static Map<String, String> encoded = new HashMap<>();

    static {
        load("star", "Gwiazda");
        load("heart", "Serce");
        load("note", "Nuta");
        load("attachment", "Załącznik");
        load("help", "Pomoc");
        load("info", "Informacja");
        load("warning", "Ostrzeżenie");
        load("bug", "Bug");
    }

    private static void load(String s, String n) {
        names.add(s);
        displayNames.put(s, n);
        InputStream is = Stickers.class.getResourceAsStream("" + s + ".png");
        if (is == null) {
            return;
        }
        images.put(s, new Image(is));

        try {
            is = Stickers.class.getResourceAsStream("" + s + ".png");
            String en = Base64.getEncoder().encodeToString(IOUtils.toByteArray(is));
            encoded.put(s, en);
        } catch (IOException ex) {
            encoded.put(s, "");
        }
    }

    public static Image getImage(String s) {
        return images.get(s);
    }

    public static String getDisplayName(String s) {
        return displayNames.getOrDefault(s, s);
    }

    public static String getBase64(String s) {
        return encoded.get(s);
    }

    public static ArrayList<String> getList() {
        return names;
    }
}
