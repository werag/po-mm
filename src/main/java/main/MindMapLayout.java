package main;

import javafx.geometry.Point2D;
import ui.Node;
import ui.Root;

import java.util.Map;

public interface MindMapLayout {
    Map<String, Point2D> layout(MindMapData data, Root rootComponent, Map<String, Node> nodeComponents);
}
