package main;

import model.Node;

import java.util.ArrayList;
import java.util.List;

public interface MindMapData
{
    String getFilePath();
    String getTitle();

    List<? extends NodeData> getChildren();
    default List<? extends NodeData> getLeftChildren() {
        return new ArrayList<>();
    };
    default List<? extends NodeData> getRightChildren() {
        return getChildren();
    };

    default MindMapLayout getLayout() { return new AutomaticLayout(); }
}
