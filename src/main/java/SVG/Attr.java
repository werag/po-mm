package SVG;

public class Attr {
    private String name;
    private String value;

    public Attr(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public Attr(String name, Double value) {
        this.name = name;
        this.value = ((Long)Math.round(value)).toString();
    }

    public String toString() {
        return this.name + "=\"" + this.value + "\"";
    }
}