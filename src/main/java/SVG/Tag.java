package SVG;

import java.util.ArrayList;
import java.util.List;

public class Tag {
    private String name;
    private String content;
    private List<Attr> attrs = new ArrayList<>();
    private List<Tag> children = new ArrayList<>();

    public Tag(String name) {
        this.name = name;
    }

    public void add(Attr a) {
        attrs.add(a);
    }

    public void add(Tag t) {
        children.add(t);
    }

    public void set(String c) {
        this.content = c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(name);

        for (Attr a : attrs) {
            sb.append(" ").append(a);
        }

        sb.append(">");

        for (Tag t : children) {
            sb.append(t);
        }

        if (content != null) sb.append(content);

        sb.append("</").append(name).append(">");

        return sb.toString();
    }
}
