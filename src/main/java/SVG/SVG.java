package SVG;

import javafx.scene.paint.Color;
import main.Colors;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;

public class SVG {
    private Tag root;

    public SVG(Double width, Double height) {
        root = new Tag("svg");
        root.add(new Attr("version", "1.1"));
        root.add(new Attr("baseProfile", "full"));
        root.add(new Attr("xmlns", "http://www.w3.org/2000/svg"));
        root.add(new Attr("xmlns:xlink", "http://www.w3.org/1999/xlink"));
        root.add(new Attr("width", width));
        root.add(new Attr("height", height));

        try {
            InputStream is = SVG.class.getResourceAsStream("ssp");
            String fonts = IOUtils.toString(is, "UTF-8");
            Tag defs = new Tag("style");
            defs.set(fonts);
            root.add(defs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Tag makeText(Double x, Double y, String t, Color c) {
        return makeText(x, y, t, c, false);
    }

    public static Tag makeText(Double x, Double y, String t, Color c, Boolean bold) {
        Tag text = new Tag("text");
        text.add(new Attr("x", x));
        text.add(new Attr("y", y));
        text.set(t);
        text.add(new Attr("fill", Colors.toHex(c)));
        text.add(new Attr("font-family", "Source Sans Pro" + (bold ? " Bold" : "")));
        text.add(new Attr("font-size", "15px"));
        return text;
    }

    public static Tag makeRectangle(Double x, Double y, Double w, Double h, Color c) {
        Tag rect = new Tag("rect");
        rect.add(new Attr("x", x));
        rect.add(new Attr("y", y));
        rect.add(new Attr("width", w));
        rect.add(new Attr("height", h));
        rect.add(new Attr("fill", Colors.toHex(c)));

        return rect;
    }

    public String toString() {
        return root.toString();
    }

    public void addElement(Tag e) {
        root.add(e);
    }

    private static String detectFormat(String content) {
         switch (content.substring(0, 4)) {
             case "iVBO": return "png";
             case "/j9/": return "jpeg";
             default: return "jpeg";
         }
    }

    public static Tag makeImage(double x, double y, double w, double h, String content) {
        Tag res = new Tag("image");
        res.add(new Attr("x", x));
        res.add(new Attr("y", y));
        res.add(new Attr("width", w));
        res.add(new Attr("height", h));
        res.add(new Attr("xlink:href", "data:image/" + detectFormat(content) + ";base64," + content));
        return res;
    }
}
