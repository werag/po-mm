import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.EditDescription;
import main.NodeData;
import ui.NodeEdit;

import java.util.ArrayList;

public class NodeEditingTest extends Application {

    @Override
    public void start(Stage stage) {
        NodeData data = new NodeData() {
            @Override
            public String getLabel() { return "First label"; }

            @Override
            public String getID() { return "id"; }

            @Override
            public ArrayList<? extends NodeData> getChildren() { return null; }

            @Override
            public String getImage() { return null; }
        };

        NodeEdit editor = new NodeEdit(data);

        editor.setOnSave((String id, EditDescription description) -> {
            System.out.println("Node '" + id + "' saved:");
            if (description.label != null)
                System.out.println("    " + description.label.getType() + ", " + description.label.getContent());
            if (description.image != null)
                System.out.println("    " + description.image.getType() + ", " + (description.image.getContent() != null ? description.image.getContent().substring(0, 10) : "null"));
            if (description.color != null)
                System.out.println("    " + description.color.getType() + ", " + description.color.getContent());
        });

        editor.setOnClose(() -> {
            System.out.println("Node edit canceled");
        });

        StackPane root = new StackPane();
        root.setBackground(new Background(new BackgroundFill(Color.web("#444444"), null, null)));
        root.setAlignment(Pos.CENTER);
        root.getChildren().add(editor);
        Scene scene = new Scene(root, 500, 500);
        stage.setTitle("Node Editing Test");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String... args) {
        launch(args);
    }
}
