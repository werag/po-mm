import javafx.application.Application;
import javafx.stage.Stage;
import main.View;

public class MapLayoutTest extends Application {
    public void start(Stage stage) {
        View v = new View(stage);

        v.pushUpdate(new MockMindMap());
    }

    public static void main(String... args) { launch(args); }
}
