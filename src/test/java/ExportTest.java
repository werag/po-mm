import javafx.application.Application;
import javafx.stage.Stage;
import main.Controller;
import main.MindMapEditor;
import main.View;
import model.Model;
import org.json.simple.parser.ParseException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ExportTest extends Application {

    public void start(Stage stage) throws IOException, ParseException {
        String ipath = "src/test/resources/pictures.mmap";
        String opath = "exported.svg";

        Model m = new Model();
        View v = new View(stage);

        m.requestUpdates(v);
        m.loadFile(ipath);

        FileWriter file = new FileWriter(opath);
        file.write(v.exportString());
        file.flush();
        file.close();

        stage.close();
    }

    public static void main(String... args) {
        launch(args);
    }
}
