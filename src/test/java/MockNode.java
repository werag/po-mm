import main.Colors;
import main.NodeData;

import java.util.ArrayList;

public class MockNode implements NodeData {
    private String label;
    private ArrayList<NodeData> children = new ArrayList<NodeData>();
    static int nextID = 0;
    private String id = Integer.valueOf(nextID++).toString();

    public String getLabel() {
        return label;
    }
    public String getID() { return id; }

    public ArrayList<NodeData> getChildren() {
        return children;
    }

    @Override
    public String getImage() {
        return null;
    }

    public MockNode(String label, MockNode... children) {
        this.label = label;

        for (MockNode n : children) {
            this.children.add(n);
        }
    }

    public Integer getColor() {
        return Colors.random();
    }
}
