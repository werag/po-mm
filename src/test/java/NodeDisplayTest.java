import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.NodeData;
import ui.Node;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class NodeDisplayTest extends Application {
    private static final String label = "Label - Culpa irure";
    private static final String path = "src/test/resources/water.txt";
    private static String image = null;

    @Override
    public void start(Stage stage) {
        try {
            Scanner in = new Scanner(new FileReader(path));
            image = in.nextLine();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            Platform.exit();
        }

        Pane root = new Pane();
        root.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"), null, null)));

        Node a = new Node(new NodeData() {
            @Override
            public String getLabel() {
                return label;
            }

            @Override
            public String getID() {
                return "0";
            }

            @Override
            public ArrayList<NodeData> getChildren() {
                return new ArrayList<>();
            }

            @Override
            public String getImage() {
                return image;
            }
        });

        a.relocate(100, 150);
        root.getChildren().add(a);

        Scene scene = new Scene(root, 500, 500);
        stage.setTitle("UI Components Test");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String... args) {
        launch(args);
    }
}
