import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.EditDescription;
import main.MindMapData;
import main.NodeData;
import ui.Root;

import java.util.ArrayList;

public class RootDisplayTest extends Application {

    @Override
    public void start(Stage stage) {
        Pane root = new Pane();
        root.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"), null, null)));

        MindMapData dataa = new MindMapData() {
            @Override
            public String getFilePath() { return null; }

            @Override
            public String getTitle() { return "Mapa myśli"; }

            @Override
            public ArrayList<? extends NodeData> getChildren() { return null; }

        };

        MindMapData datab = new MindMapData() {
            @Override
            public String getFilePath() { return null; }

            @Override
            public String getTitle() {
                return "Mapa myśli z długim tytułem Laborum anim ea sit "
                    + "sint eiusmod tempor aliquip aliqua duis";
            }

            @Override
            public ArrayList<? extends NodeData> getChildren() { return null; }

        };

        Root compa = new Root(dataa);
        Root compb = new Root(datab);
        compa.relocate(150, 50);
        compb.relocate(150, 250);

        compa.setOnRootClicked(() -> System.out.println("A clicked"));
        compb.setOnRootClicked(() -> System.out.println("B clicked"));

        root.getChildren().addAll(compa, compb);
        Scene scene = new Scene(root, 500, 500);
        stage.setTitle("Root Display Test");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String... args) {
        launch(args);
    }
}
