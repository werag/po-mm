import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.NodeData;
import ui.Edge;
import ui.Node;

import java.util.ArrayList;

public class UIComponentsTest extends Application {

    @Override
    public void start(Stage stage) {
        Pane root = new Pane();
        root.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"), null, null)));

        Node a = new Node(new NodeData() {
            @Override
            public String getLabel() {
                return "label";
            }

            @Override
            public String getID() {
                return "0";
            }

            @Override
            public ArrayList<NodeData> getChildren() {
                return new ArrayList<>();
            }

            @Override
            public String getImage() {
                return null;
            }
        });
        a.relocate(100, 150);

        Node b = new Node(
            new NodeData() {
                @Override
                public String getLabel() {
                    return "A little\nlonger label";
                }

                @Override
                public String getID() {
                    return "1";
                }

                @Override
                public ArrayList<NodeData> getChildren() {
                    return new ArrayList<>();
                }

                @Override
                public String getImage() {
                    return null;
                }
            });
        b.relocate(150, 230);

        Edge e = new Edge(a, b);
        root.getChildren().addAll(e, a, b);

        Scene scene = new Scene(root, 500, 500);
        stage.setTitle("UI Components Test");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String... args) {
        launch(args);
    }
}
