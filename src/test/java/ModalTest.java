import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import main.EditDescription;
import main.MindMapData;
import main.NodeData;
import ui.Modal;
import ui.Root;

import java.util.ArrayList;

public class ModalTest extends Application {

    @Override
    public void start(Stage stage) {
        StackPane root = new StackPane();
        root.setBackground(new Background(new BackgroundFill(Color.web("#eeeeee"), null, null)));

        Modal modal = new Modal();
        VBox box = new VBox();
        Text text = new Text(20, 20, "Test");
        box.getChildren().add(text);
        box.setPadding(new Insets(20));
        modal.setContent(box);
        modal.show();

        root.getChildren().addAll(modal);

        Scene scene = new Scene(root, 500, 500);
        stage.setTitle("Modal Test");
        stage.setScene(scene);
        stage.show();

    }

    public static void main(String... args) {
        launch(args);
    }
}
