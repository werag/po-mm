import main.MindMapData;
import main.NodeData;

import java.util.ArrayList;
import java.util.List;

public class MockMindMap implements MindMapData {
    private ArrayList<NodeData> children = new ArrayList<NodeData>();

    public MockMindMap() {
        this.children.add(new MockNode("alpha"));

        this.children.add(new MockNode("beta",
            new MockNode("beta.first"),
            new MockNode("beta.second")
        ));

        this.children.add(new MockNode("gamma",
            new MockNode("gamma.first"),
            new MockNode("gamma.\nsecond",
                new MockNode("gamma.second.first"),
                new MockNode("gamma.second.")
            ),
            new MockNode("C.third")
        ));

        this.children.add(new MockNode("D"));
        this.children.add(new MockNode("E",
            new MockNode("E.first",
                new MockNode("E.first.first")
            ),
            new MockNode("E.second"),
            new MockNode("E.third",
                new MockNode("E.third.first"),
                new MockNode("E.third.second")
            ),
            new MockNode("E.fourth")
        ));
    }

    public String getFilePath() {
        return "";
    }

    public String getTitle() {
        return "Test Mind Map";
    }

    public ArrayList<NodeData> getChildren() {
        return this.children;
    }

    @Override
    public List<? extends NodeData> getLeftChildren() {
        return null;
    }

    @Override
    public List<? extends NodeData> getRightChildren() {
        return null;
    }
}

